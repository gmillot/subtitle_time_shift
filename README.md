[//]: # "#to make links in gitlab: example with racon https://github.com/isovic/racon"
[//]: # "tricks in markdown: https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown"

| usage | R dependencies |
| --- | --- |
| [![R Version](https://img.shields.io/badge/code-R-blue?style=plastic)](https://cran.r-project.org/mirrors.html) | [![Dependencies: R Version](https://img.shields.io/badge/R-v4.1.2-blue?style=plastic)](https://cran.r-project.org/mirrors.html) |
| [![License: GPL-3.0](https://img.shields.io/badge/licence-GPL%20(%3E%3D3)-green?style=plastic)](https://www.gnu.org/licenses) | |



## TABLE OF CONTENTS

   - [AIM](#aim)
   - [CONTENT](#content)
   - [HOW TO RUN](#how-to-run)
   - [OUTPUT](#output)
   - [VERSIONS](#versions)
   - [LICENCE](#licence)
   - [CITATION](#citation)
   - [CREDITS](#credits)
   - [WHAT'S NEW IN](#what's-new-in)


## AIM

Shift the time of .srt subtitle files


## CONTENT

**subtitle_time_shift.R** file that can be executed using a CLI (command line interface) or sourced in R or RStudio.

**dataset** folder containing some datasets than can be used as examples (from https://en.wikipedia.org/wiki/SubRip)

**example_of_result** folder containing an example of result obtained


## HOW TO RUN

### Using a R GUI (graphic user interface, i.e., R or RStudio windows)

1) Open the subtitle_time_shift.R file and set the parameters (in the "Parameters set by users" section)

2) Open R or RStudio

3) Source the subtitle_time_shift.R file, for instance using the following instruction:

	`  source("C:/Users/Gael/Desktop/subtitle_time_shift.R")  `


### Using a R CLI (command line interface)

1) Open the subtitle_time_shift.R file and set the parameters (2 first lines)

2) Open a shell windows

3) run subtitle_time_shift.R, for instance using the following instruction:

	`  Rscript subtitle_time_shift.R  `

For cygwin, use something like:

`  /cygdrive/c/Program\ Files/R/R-4.0.2/bin/Rscript subtitle_time_shift.R  `


## OUTPUT

The same file in the same directory but with time shifted


## VERSIONS

The different releases are tagged [here](https://gitlab.pasteur.fr/gmillot/sub_shift/-/tags)


## LICENCE

This package of scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchandability or fitness for a particular purpose.
See the GNU General Public License for more details at https://www.gnu.org/licenses or in the Licence.txt attached file.


## CITATION

Not yet published


## CREDITS

[Gael A. Millot](https://gitlab.pasteur.fr/gmillot), Hub-CBD, Institut Pasteur, USR 3756 IP CNRS, Paris, France


## WHAT'S NEW IN

### v1.3

README file improved.


### v1.2

README and subtitle_time_shift.R files improved.


### v1.1

README file improved.


### v1.0

Everything.



